owner = ND7
controller = ND7
add_core = ND7
religion = nordic_pantheon
culture = nord
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Raketa"
is_city = yes


57.1.1 = {
    owner = ND8
    controller = ND8
    add_core = ND8
}

272.1.1 = {
    owner = NA8
    controller = NA8
    add_core = NA8
    religion = nordic_pantheon
    culture = nord
}
535.1.1 = {
}

862.1.1 = {
}

1177.1.1 = {
}

1466.1.1 = {
}

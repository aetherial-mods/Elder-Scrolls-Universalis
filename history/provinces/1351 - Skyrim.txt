owner = NB8
controller = NB8
add_core = NB8
religion = nordic_pantheon
culture = nord
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Illdund"
is_city = yes



57.1.1 = {
   owner = NC5
   controller = NC5
   add_core = NC5
}

272.1.1 = {
   owner = REA
   controller = REA
   add_core = REA
}

535.1.1 = {
}

862.1.1 = {
}

1177.1.1 = {
}

1466.1.1 = {
}

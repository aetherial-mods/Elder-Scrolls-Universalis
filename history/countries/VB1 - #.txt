government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = hollow
add_accepted_culture = bosmer
capital = 863







54.1.1 = {
	monarch = {
		name = "Ostion"
		dynasty = "the Wilderking"
		birth_date = 18.1.1
		culture = altmer
		adm = 6
		dip = 1
		mil = 3
    }

}

272.1.1 = {
	monarch = {
		name = "Orchinar"
		dynasty = "the Wilderking"
		birth_date = 249.1.1
		adm = 6
		dip = 1
		mil = 6
    }

}

535.1.1 = {
	monarch = {
		name = "Peruin"
		dynasty = "the Wilderking"
		birth_date = 505.1.1
		adm = 1
		dip = 4
		mil = 1
        female = yes
    }
}

862.1.1 = {
	monarch = {
		name = "Lorchon"
		dynasty = "the Wilderking"
		birth_date = 833.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

1177.1.1 = {
	monarch = {
		name = "Indiir"
		dynasty = "the Wilderking"
		birth_date = 1157.1.1
		adm = 1
		dip = 5
		mil = 4
    }

}

1466.1.1 = {
	monarch = {
		name = "Ramsi"
		dynasty = "the Wilderking"
		birth_date = 1430.1.1
		adm = 4
		dip = 6
		mil = 6
    }

}


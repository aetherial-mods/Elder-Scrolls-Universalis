dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UB0
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = OA7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = OA1
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = OA3
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = OA9
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA1
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA2
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA3
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA8
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA6
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA4
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = OA8
	second = UA5
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = OA1
	second = OA3
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = OA1
	second = OA7
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = UB0
	second = OA3
	start_date = 57.1.1
	end_date = 9999.1.1
}
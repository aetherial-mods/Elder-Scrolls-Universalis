NDefines.NGame.START_DATE = "54.1.1"
NDefines.NGame.END_DATE = "9999.12.31"
NDefines.NGame.MAX_RANDOM_NEW_WORLD = "54.1.1"
NDefines.NGame.GREAT_POWER_FRENZY_START = "9999.12.31"	-- Latest date for the start of the Great Power Frenzy (struggle).
NDefines.NGame.GREAT_POWER_FRENZY_WARMUP_MONTHS = 1200	-- How many months does it take for the Great Power Frenzy to reach full potency after starting.
NDefines.NGame.GREAT_POWER_FRENZY_QUICKSTART = 1		-- Can the Great Power Frenzy start early due to one alliance block becoming too powerful
NDefines.NGame.AGE_USHER_IN_TIME = 180				-- how many months progress for a new age needs.
NDefines.NGame.MAX_COLONIAL_NATIONS = 100				-- Max is 100
NDefines.NGame.MAX_CLIENT_STATES = 15				-- Max is 100 -- TODO Lower this to 75 after 1.18 (used to start at K75 for some reason)
NDefines.NGame.MAX_ESTATE_COUNTRIES = 100				-- Max is 100
NDefines.NGame.MAX_TRADING_CITIES = 100				-- Max is 100
NDefines.NGame.MAX_CUSTOM_COUNTRIES = 100				-- Max is 100
NDefines.NGame.MAX_OBSERVERS = 100						-- Max is 100

NDefines.NGame.MERC_COMPANIES_PER_RNW_REGION = 5 		-- auto-generated merc companies per region in RNW
NDefines.NGame.RNW_MERC_COMPANY_MODIFIER_CHANCE = 0.33 -- chance of a modifier being applied to a RNW mercenary company
NDefines.NGame.RNW_MERC_COMPANY_DEV_FACTOR_MIN = 0.01 	-- min regiments_per_development
NDefines.NGame.RNW_MERC_COMPANY_DEV_FACTOR_MAX = 0.05 	-- max regiments_per_development
NDefines.NGame.RNW_MERC_COMPANY_CAVALRY_MIN = 0.1 	-- min cavalry_weight if we're using cavalry
NDefines.NGame.RNW_MERC_COMPANY_CAVALRY_MAX = 0.25	-- max cavalry_weight if we're using cavalry
NDefines.NGame.RNW_MERC_COMPANY_ARTILLERY_MIN = 0.1	-- min artillery_weight if we're using artillery
NDefines.NGame.RNW_MERC_COMPANY_ARTILLERY_MAX = 0.25	-- max artillery_weight if we're using artillery
NDefines.NGame.RNW_MERC_COMPANY_RANDOM_NAME_COUNT = 20 -- RANDOM_MERCENARY_NAME + number up to this value

NDefines.NGame.MAX_FEDERATION_COUNTRIES = 100

NDefines.NEngine.EVENT_PROCESS_OFFSET = 50

NDefines.NGraphics.SHIELD_GLOW_RANGE = 1.0						-- Set to 0 to disable Great Power shield glow set to 1 for epileptic seizure.

NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_EMPEROR_R = 0.5
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_EMPEROR_G = 0.0
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_EMPEROR_B = 0.25

NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_ELECTOR_R = 0.75
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_ELECTOR_G = 0.25
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_ELECTOR_B = 0.0

NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_FREE_CITY_R = 0.0
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_FREE_CITY_G = 0.0
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_FREE_CITY_B = 0.75

NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_DEFAULT_R = 0.0
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_DEFAULT_G = 0.5
NDefines.NGraphics.MAPMODE_EMPIRE_PROVINCE_FOR_DEFAULT_B = 0.0

NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_RELIGION_R = 0.0
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_RELIGION_G = 0.5
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_RELIGION_B = 0.0

NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_HERETIC_RELIGION_R = 0.0
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_HERETIC_RELIGION_G = 0.0
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_HERETIC_RELIGION_B = 0.5

NDefines.NGraphics.MAPMODE_PERPETUAL_DIET_LOCATION_R = 0.2
NDefines.NGraphics.MAPMODE_PERPETUAL_DIET_LOCATION_G = 0.8
NDefines.NGraphics.MAPMODE_PERPETUAL_DIET_LOCATION_B = 0.8

NDefines.NGraphics.AGGRESSIVE_EXPANSION_RED_R = 1.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_RED_G = 0.2
NDefines.NGraphics.AGGRESSIVE_EXPANSION_RED_B = 0.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_RED_A = 1.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_AMBER_R = 1.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_AMBER_G = 0.95
NDefines.NGraphics.AGGRESSIVE_EXPANSION_AMBER_B = 0.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_AMBER_A = 1.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_GREEN_R = 0.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_GREEN_G = 0.95
NDefines.NGraphics.AGGRESSIVE_EXPANSION_GREEN_B = 0.0
NDefines.NGraphics.AGGRESSIVE_EXPANSION_GREEN_A = 1.0

NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_RELIGION_LEADER_R = 0.2
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_RELIGION_LEADER_G = 0.7
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_RELIGION_LEADER_B = 0.2

NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_HERETIC_RELIGION_LEADER_R = 0.2
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_HERETIC_RELIGION_LEADER_G = 0.2
NDefines.NGraphics.MAPMODE_RELIGIOUS_LEAGUE_HERETIC_RELIGION_LEADER_B = 0.7

NDefines.NFrontend.FRONTEND_POS_X = 1238.0
NDefines.NFrontend.FRONTEND_POS_Y = 1500.0
NDefines.NFrontend.FRONTEND_POS_Z = 1575.0
NDefines.NFrontend.FRONTEND_LOOK_X = 1238.0
NDefines.NFrontend.FRONTEND_LOOK_Y = 0.0
NDefines.NFrontend.FRONTEND_LOOK_Z = 1575.0
NDefines.NFrontend.CAMERA_MIN_HEIGHT = 75
NDefines.NFrontend.CAMERA_MAX_HEIGHT = 2211

NDefines.NGui.AUTO_SELECT_EVENT_MONTHS = 5
NDefines.NGui.LEDGER_GUI_OBJECT_OFFSET = -5
NDefines.NGui.LEDGER_RIGHT_ALIGNED_COLUMN_PADDING = 20

NDefines.NAIEconomy.LOAN_REPAYMENT_SAVINGS_PRIORITY_WARTIME = 0.5
NDefines.NAIEconomy.BASE_SAVINGS_PRIORITY = 0.5
NDefines.NAIEconomy.BASE_SAVINGS_PRIORITY_WARTIME = 0.0
NDefines.NAIEconomy.SUBSIDY_MAX_BUDGET_FRACTION = 0.05

NDefines.NAI.REPAY_LOAN_BASE_AI_DESIRE = 2500.0
NDefines.NAI.DEBASE_THRESHOLD = 1000
NDefines.NAI.UPGRADE_CENTER_OF_TRADE_BASE_AI_DESIRE = 150.0
NDefines.NAI.UPGRADE_CENTER_OF_TRADE_AI_POWER_DESIRE = 25.0
NDefines.NAI.DEFICIT_SPENDING_MIN_MONTHS = 5
NDefines.NAI.DEFICIT_SPENDING_MIN_MONTHS_PEACETIME = 25
NDefines.NAI.FORCE_COMPOSITION_CHANGE_TECH_LEVEL = 70

NDefines.NAI.DANGEROUS_OVEREXTENSION_PERCENTAGE = 0.1

NDefines.NAI.OVERLORD_VITAL_PROVINCE_CLAIM_BIAS = 500
NDefines.NAI.OVERLORD_INTERESTING_PROVINCE_BIAS = 250
NDefines.NAI.GREAT_PROJECT_DESIRE_CAPITAL_MODIFIER = 3
NDefines.NAI.GREAT_PROJECT_DESIRE_CAPITAL_AREA_MODIFIER = 3
NDefines.NAI.ACCEPTABLE_BALANCE_MULT_FRIEND_IN_COMBAT = 0.75
NDefines.NAI.FORT_MAINTENANCE_CHEAT = 0

NDefines.NAI.ADVISOR_BUDGET_FRACTION_MAX = 0.50
NDefines.NAI.ADVISOR_BUDGET_FRACTION_MIN = 0.25
NDefines.NAI.ADVISOR_BUDGET_FRACTION_MERITOCRACY_MAX = 0.75
NDefines.NAI.ADVISOR_BUDGET_FRACTION_MERITOCRACY_MIN = 0.35
NDefines.NAI.ADVISOR_BUDGET_THRESHOLD = 1.25
NDefines.NAI.STATE_MAINTENANCE_BUDGET_FRACTION = 0.15
	
NDefines.NAI.MILITARISE_FORT_BUDGET_FACTOR = 1.5

NDefines.NAI.PEACE_TERMS_HUMILIATE_RIVAL_BASE_MULT = 0.0
NDefines.NAI.DRILLING_BUDGET_OF_SURPLUS = 0.5

NDefines.NAI.CHARTER_COMPANY_BASE_RELUCTANCE = -50			
NDefines.NAI.CHARTER_COMPANY_DEVELOPMENT_RELUCTANCE_MULTIPLIER = 1.5
NDefines.NAI.CHARTER_COMPANY_GREAT_PROJECT_VALUE_RELUCTANCE = 0.1	
NDefines.NAI.CHARTER_COMPANY_RESTRICTED_REGION_BASE = 50
NDefines.NAI.CHARTER_COMPANY_RESTRICTED_REGION_LESS_DEVELOPMENT_PENALTY = 25
NDefines.NAI.CHARTER_COMPANY_RESTRICTED_REGION_LESS_INSTITUTIONS_PENALTY = 50
NDefines.NAI.CHARTER_COMPANY_SWEETENER_PERCENTAGE_MULTIPLIER = 100
NDefines.NAI.CHARTER_COMPANY_ALLIANCE_BONUS = 50
NDefines.NAI.CHARTER_COMPANY_DIPLO_REP_MULTIPLIER = 2.5
NDefines.NAI.CHARTER_COMPANY_OPINION_MULTIPLIER = 0.25
NDefines.NAI.CHARTER_COMPANY_THREATENED_PENALTY = 100
NDefines.NAI.CHARTER_COMPANY_NON_TRADE_LEAGUE_MEMBER_PENALTY = 50
NDefines.NAI.CHARTER_COMPANY_EMPIRE_PENALTY = 1000
NDefines.NAI.CHARTER_COMPANY_RIVAL_PENALTY = 1000

NDefines.NAI.ESTATE_PRIVILEGE_REVOKE_THRESHOLD = 5.01
NDefines.NAI.ESTATE_PRIVILEGE_GRANT_THRESHOLD = 9.99
NDefines.NAI.ESTATE_PRIVILEGE_LAST_PENALTY = 3								
NDefines.NAI.ESTATE_INTERACTION_THRESHOLD = 49.9
NDefines.NAI.ESTATE_MAX_WANTED_INFLUENCE = 75.0
NDefines.NAI.ESTATE_MIN_WANTED_CROWNLAND = 33.0
NDefines.NAI.ESTATE_MAX_PRIVILEDGES = 4

NDefines.NAI.CALL_ECUMENICAL_COUNCIL_BASE_AI_DESIRE = 0.0
	
NDefines.NAI.GOVERNING_CAPACITY_REFORM_PROGRESS_GAP_TOLERANCE = 50
NDefines.NAI.GOVERNING_CAPACITY_REFORM_BIAS_IF_NEEDED = 100

NDefines.NAI.ABANDON_COLONY_SIZE_THRESHOLD = 0.75
NDefines.NAI.ACCEPTABLE_BALANCE_MULT_OFFENSIVE = 0.75 -- Unless friend in combat is already applied
NDefines.NAI.FORT_MOTHBALL_SAFETY_DISTANCE = 5 -- How far from the border AI will mothball forts

NDefines.NAI.ENEMY_ALLIANCE_PENALTY = 35 -- Penalty on alliance chance for being allied to enemies
	
NDefines.NAI.CONVERT_TRIBUTARY_TO_VASSAL_AI_DESIRE_BASE = 25
NDefines.NAI.CONVERT_TRIBUTARY_TO_VASSAL_AI_DESIRE_PREPARING_FOR_WAR_SCORE = 35
NDefines.NAI.CONVERT_TRIBUTARY_TO_VASSAL_AI_DESIRE_WANTS_LAND_SCORE = 35

-- AI parameters for not considering diplomacy with all other countries every day (for performance)
NDefines.NAI.FOREIGN_MINISTER_IGNORE_DISTANCE_BASE = 10
NDefines.NAI.FOREIGN_MINISTER_BASE_PROVINCE_COUNT = 5
	
NDefines.NAI.FIRE_ADVISOR_LOAN_REQUIREMENT = 5							-- AI will not fire advisors until it has this many loans
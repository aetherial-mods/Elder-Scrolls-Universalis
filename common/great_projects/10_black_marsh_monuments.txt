# xinchei_konu
# bogmother
# ten_maur_wolk
# ixtaxh_thitithil_meht
# murkwood
# sunscale_strand
# shrine_of_the_black_maw
# blackrose_prison

blackrose_prison = {
	start = 6745
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { min_autonomy_in_territories = -0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { min_autonomy_in_territories = -0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { min_autonomy_in_territories = -0.15 }
		on_upgraded = { }
	}
}

shrine_of_the_black_maw = {
	start = 6532
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			OR = {
				has_reform = slavery
				has_reform = raiding
				culture_group = dead_cg
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			OR = {
				has_reform = slavery
				has_reform = raiding
				culture_group = dead_cg
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			OR = {
				has_reform = slavery
				has_reform = raiding
				culture_group = dead_cg
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { build_cost = -0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { build_cost = -0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { build_cost = -0.15 }
		on_upgraded = { }
	}
}

murkwood = {
	start = 6644
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { hostile_attrition = 0.5 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { hostile_attrition = 1.0 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { hostile_attrition = 1.5 }
		on_upgraded = { }
	}
}

sunscale_strand = {
	start = 6634
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			OR = {
				religion_group = daedric_group
				religion_group = elemental_group
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			OR = {
				religion_group = daedric_group
				religion_group = elemental_group
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			OR = {
				religion_group = daedric_group
				religion_group = elemental_group
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { core_decay_on_your_own = -0.15 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { core_decay_on_your_own = -0.35 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { core_decay_on_your_own = -0.50 }
		on_upgraded = { }
	}
}

ixtaxh_thitithil_meht = {
	start = 6820
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			OR = {
				religion = hist
				culture_group = lilmothiit_cg
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			OR = {
				religion = hist
				culture_group = lilmothiit_cg
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			OR = {
				religion = hist
				culture_group = lilmothiit_cg
			}
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { ae_impact = -0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { ae_impact = -0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { ae_impact = -0.15 }
		on_upgraded = { }
	}
}

ten_maur_wolk = {
	start = 6627
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			culture_group = marsh_men_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			culture_group = marsh_men_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			culture_group = marsh_men_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { adm_advisor_cost = -0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { adm_advisor_cost = -0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { adm_advisor_cost = -0.15 }
		on_upgraded = { }
	}
}

bogmother = {
	start = 1077
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			culture_group = marsh_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			culture_group = marsh_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			culture_group = marsh_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { tolerance_heathen = 0.5 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { tolerance_heathen = 1.0 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { tolerance_heathen = 1.5 }
		on_upgraded = { }
	}
}

xinchei_konu = {
	start = 1110
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		owner = {
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { trade_goods_size_modifier = 0.15 }
		country_modifiers = { production_efficiency = 0.1 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { trade_goods_size_modifier = 0.30 }
		country_modifiers = { production_efficiency = 0.15 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { trade_goods_size_modifier = 0.5 }
		country_modifiers = { production_efficiency = 0.2 }
		on_upgraded = { }
	}
}
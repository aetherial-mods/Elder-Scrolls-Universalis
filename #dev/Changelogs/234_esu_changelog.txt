##############################
Version: 2.3.4
Date: 27.02.22
EU4 Version: 1.32.2
##############################

### Additions ###
- Sload mission tree
- Added option to hide the Sacking Event
- Added 7 new flavour events
- Added 20 new Trade Goods

### Changes ###
- Skykiller Gallery now provides dev cost decrease bonus
- Strong Trade Companies now provide governing and reform progress bonuses
- Reduced base liberty desire reduction for all subject types

### Fixes ###
- Falmers should no longer spawn in Morrowind
- Fixed Doom mechanics for Pyandocean nations
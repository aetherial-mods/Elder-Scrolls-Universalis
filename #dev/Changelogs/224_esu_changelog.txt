##############################
Version: 2.2.4
Date: 12.11.21
EU4 Version: 1.32.*
##############################

### Additions ###
- Atmoran Missions
- Overhaul of Isolationism for Tsaesci
- New event to create Roscrea as Personal Union (Part of Atmoran Mission Tree)
- New Mission tree for Glacial nations.
- New Mission tree for Sea Giants nations.
- New Mission tree for Frost Giants nations.
- New Mission tree for Giants nations.
- 5 New events for Nordic Pantheon
- 2 New events for All-Maker Cult
- 1 New event for Hist religion
- 6 New events for religions in Elemental group.
- Added interctions with estates via flvaour religion events.
- Castle Volkihar Monument
- Integration of Peace Treaties Expanded
- 16 new religion random events
- 6 new religion random events for Akavir nations
- New CB for Tsaesci - less peace cost, but more AE
- New Terrain - Orcish Stronghold 
- 7 New High Rock Monuments
- 3 new Decisions for Soul Schriven nations, featuring culture convertion, reverse convertion and liberation from Molag Bal's influence.
- Addition of Cawa units as the Noible Warriors

### Changes ###
- 1.32.* Compatibility changes of institutions and technology
- Government reform balancing
- Players from Nirn cannot ask for military access to Oblivion nations and vice versa
- Governments with Militarism mechanics no longer get gov capacity bonuses from government reform type
- Militarism is no longer limited by province number, but by governmental capacity
- Changed unit pips so that defensive/offensive versions of each unit are balanced.
- Rebalance of Karma modifiers.
- Health mechanics is now relying on ruler lifespan modifier
- Increased the opinion penalties for heretic/heathen religions. 
- Update of the Thumbnail to 1.32
- Nerf of Manpower Pool and Manpower Recovery Speed modifiers
- Decrease in governemnt capacity from technologies
- Church power (Protestant mechanics) gain is reduced, the cost of Chirch Aspects is increased. Sloads faith's aspect cost is rebalance to meet those changes. 
- Nerf of Religion Unity modifiers
- Buildings now increase development, when the are built

### Fixes ###
- Fixed weird wasteland visibility
- Fixed various typos in localisation
- Fixed the Aetherium trade good event only firing for players
- Fixed some errors in the river map
- Fixed too long names for Missions
- Fixed province highlighting for Skyrim Missions
- Fixed the Portal bug, when players could cross it if they ask for military access
- Fix of the AI, declaring war, when he has internal problems 
- Fix of Dwemer Disappearence event - added 3 Dwemer cultures there
- Fix of Vassal Vs Vassal wars, that could ended in release of independent nation or return of cores to independent nations
- Fix of Culture based opinion mechanics
- Fixed a typo in institution event titles
- Fix of Opinion Mechanics
- Various small bugfixes
- Fix of spelling errors
- Fixed TI around Red Mountain
- Fixed Nethar (1195)'s name (hopefully)
- Fixed faction influence modifier icons
- Fix of 7, 8, 9, 10, 11 Scenarios
- Removed all forbidden tags
- Fixed issue with the Ritual Birthsign's aspect modifiers
- Fixed missing vampire flavour event
- Fix of Custom Ideas for randomly generated nations
- Fix of trade goods bonuses for Dwemer Metal and 


### Deletions ###
- Removed remnants of Kamal from the map
- Removed French localisation as it was working poorly. It will be eplaced with English until a proper fix would be provided.
- Removed old institution birthplace modifiers